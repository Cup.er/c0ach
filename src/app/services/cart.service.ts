import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CardModelPublic, CartModelServer } from '../models/cart.model';
import { ProductModelServer } from '../models/product.model';
import { OrderService } from './order.service';
import { ProductService } from './product.service';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  private serverUrl = environment.SERVER_URL;
  

  // data variable to store the cart information on the client's local storage
  private cartDataClient: CardModelPublic = {
    total: 0,
    prodData: [{
      incart: 0,
      id: 0,

    }],
  }

  // data variable to store cart information on the server
  private cartDataServer: CartModelServer = {
    total: 0,
    data: [{
      numInCart: 0,
      product: undefined,
    }]
  }

  //observable for the components to subscribe
  cartTotal$ =  new BehaviorSubject<number>(0);
  cartData$ = new BehaviorSubject<CartModelServer>(this.cartDataServer);



  constructor(private http: HttpClient,
              private productService: ProductService,
              private orderService: OrderService,
              private router: Router,
              private toast: ToastrService,
              private spinner: NgxSpinnerService) { 

      this.cartTotal$.next(this.cartDataServer.total);
      this.cartData$.next(this.cartDataServer);
      // get the information from local storage ( if any )
      let info: CardModelPublic = JSON.parse(localStorage.getItem('cart'));
      // check if the info variable is null or has some data in it 

      if(info !== null && info !== undefined && info.prodData[0].incart !== 0){
        // Local storage is not empty and has some inforamtion
        this.cartDataClient = info;
        // loop through each entry and put it in the cardDataServer object
        this.cartDataClient.prodData.forEach(p=>{
          this.productService.getSingleProduct(p.id).subscribe((actualProductInfo: ProductModelServer) => {
             if(this.cartDataServer.data[0].numInCart === 0){
               this.cartDataServer.data[0].numInCart = p.incart;
               this.cartDataServer.data[0].product = actualProductInfo;
               // todo create CalculateTotal Function and replace it here
               this.cartDataClient.total = this.cartDataServer.total;
               localStorage.setItem('cart', JSON.stringify(this.cartDataClient));

             }
             else{
               //CartDataServer already has some entry in it
               this.cartDataServer.data.push({
                 numInCart: p.incart,
                 product: actualProductInfo,
               });
               this.CalculateTotal();
              // todo create CalculateTotal Function and replace it here
               this.cartDataClient.total = this.cartDataServer.total;
               localStorage.setItem('cart', JSON.stringify(this.cartDataClient));

             }
             this.cartData$.next({... this.cartDataServer});
              
          });
        });
        
      }
  }

  CalculateSubTotal(index): Number {
    let subTotal = 0;

    let p = this.cartDataServer.data[index];
    // @ts-ignore
    subTotal = p.product.price * p.numInCart;

    return subTotal;
  }

  AddProductToCart(id: number, quantity?: number){
    this.productService.getSingleProduct(id).subscribe(prod => {
      // 1. If the cart is empty 
      if(this.cartDataServer.data[0].product === undefined){
        this.cartDataServer.data[0].product = prod;
        this.cartDataServer.data[0].numInCart = quantity !== undefined ? quantity : 1;
        this.CalculateTotal();
      // todo calculate total amount
        this.cartDataClient.prodData[0].incart = this.cartDataServer.data[0].numInCart;
        this.cartDataClient.prodData[0].id = prod.id;
        this.cartDataClient.total = this.cartDataServer.total;
        localStorage.setItem('cart', JSON.stringify(this.cartDataClient));
        this.cartData$.next({... this.cartDataServer});
        this.toast.success(`${prod.name} added to the cart`, 'Product Added' ,{
          timeOut: 1500,
          progressBar:true,
          progressAnimation: 'increasing',
          positionClass: 'toast-top-right',
        });
      }

      // 2. IF the cart has some items
      else{
        let index = this.cartDataServer.data.findIndex(p => p.product.id === prod.id); // -1 or a positive value
      
        //  a. if that item is already in the cart ==> index is positive value
        if(index !== -1){
          if(quantity !== undefined && quantity <= prod.quantity){
            this.cartDataServer.data[index].numInCart = this.cartDataServer.data[index].numInCart < prod.quantity ? quantity: prod.quantity;
          }
          else {
            this.cartDataServer.data[index].numInCart < prod.quantity ? this.cartDataServer.data[index].numInCart++ : prod.quantity;
          }

          this.cartDataClient.prodData[index].incart = this.cartDataServer.data[index].numInCart;
          localStorage.setItem('cart', JSON.stringify(this.cartDataClient));
          // todo display a toast notification 
          this.toast.info(`${prod.name} quantity updated in the cart`, 'Product Updated' ,{
            timeOut: 1500,
            progressBar:true,
            progressAnimation: 'increasing',
            positionClass: 'toast-top-right',
          });
          this.CalculateTotal();
          //todo calculate total amount
          this.cartDataClient.total = this.cartDataServer.total;
          localStorage.setItem('cart', JSON.stringify(this.cartDataClient));
        } // end of if

        //     b. if that item is not in the cart
        else{
          this.cartDataServer.data.push({
            numInCart: 1,
            product: prod,
          });

          this.cartDataClient.prodData.push({
            incart: 1,
            id: prod.id,
          });

          localStorage.setItem('cart', JSON.stringify(this.cartDataClient));
          // todo display a toast notification 
          this.toast.success(`${prod.name} added to the cart`, 'Product Added' ,{
            timeOut: 1500,
            progressBar:true,
            progressAnimation: 'increasing',
            positionClass: 'toast-top-right',
          });
          
          this.CalculateTotal();
          //todo calculate total amount
          this.cartDataClient.total = this.cartDataServer.total;
          localStorage.setItem('cart', JSON.stringify(this.cartDataClient));
          this.cartData$.next({... this.cartDataServer});

         }  // end of else 
      }
    });
    
  }

  UpdateCartItems(index: number, increase: boolean){
    let data = this.cartDataServer.data[index];

    if(increase){
      data.numInCart < data.product.quantity ? data.numInCart++ : data.product.quantity;
      this.cartDataClient.prodData[index].incart = data.numInCart;
      this.CalculateTotal();
      //todo calculate total amount
      this.cartDataClient.total = this.cartDataServer.total;
      localStorage.setItem('cart', JSON.stringify(this.cartDataClient));
      this.cartData$.next({... this.cartDataServer});
    }
    else{
      data.numInCart--;

      if(data.numInCart < 1){
        //todo delete the product from cart
        this.DeleteProductFromCart(index);
        this.cartData$.next({... this.cartDataServer});
      }
      else{
        this.cartData$.next({... this.cartDataServer});
        this.cartDataClient.prodData[index].incart = data.numInCart;
        this.CalculateTotal();
        //todo calculate total amount
        this.cartDataClient.total = this.cartDataServer.total;
        localStorage.setItem('cart', JSON.stringify(this.cartDataClient));
      }
    }
  }
  DeleteProductFromCart(index: number){
    if(window.confirm('Are you sure you want to remove the product ?')){
      this.cartDataServer.data.splice(index, 1);
      this.cartDataClient.prodData.splice(index, 1);
      this.CalculateTotal();
      //todo calculate total amount
      this.cartDataClient.total = this.cartDataServer.total;

      if(this.cartDataClient.total === 0){
        this.cartDataClient = {total:0,prodData: [{incart:0,id:0}]};
        localStorage.setItem('cart', JSON.stringify(this.cartDataClient));

      }
      else{
        localStorage.setItem('cart', JSON.stringify(this.cartDataClient));
      }

      if(this.cartDataServer.total===0){
        this.cartDataServer = {total:0, data: [{numInCart:0, product: undefined}]};
        this.cartData$.next({... this.cartDataServer});
      }
      else{
        this.cartData$.next({... this.cartDataServer});
      }
    }
    else{
      //if the user clicks the cancel button
      return;
    }
  }

  private CalculateTotal(){
    let Total = 0;
    this.cartDataServer.data.forEach(p => {
      const {numInCart} = p;
      const {price} = p.product;
      
      Total += numInCart * price;
    
    });

    this.cartDataServer.total = Total;
    this.cartTotal$.next(this.cartDataServer.total);

  }

  ChechkoutFromCart(userId: number){
    this.http.post(`${this.serverUrl}/orders/payment`, null).subscribe((res: {success: boolean})=> {
      if(res.success){
        this.resetServerData();
        this.http.post(`${this.serverUrl}/orders/new`, {
          userId: userId,
          products: this.cartDataClient.prodData,

        }).subscribe((data: OrderResponse) => {
          this.orderService.getSingleOrder(data.order_id).then(prods =>{
            if(data.success){
              const navigationExtras : NavigationExtras = {
                state:{
                  message: data.message,
                  products: prods,
                  orderId: data.order_id,
                  total: this.cartDataClient.total
                }
              };

              // todo hide spinner
              this.spinner.hide();
              this.router.navigate(['/thankyou/'], navigationExtras).then(p =>{
                this.cartDataClient = {total: 0, prodData: [{incart:0, id:0}]};
                this.cartTotal$.next(0);
                localStorage.setItem('cart', JSON.stringify(this.cartDataClient));

              })
            }
          })
        })
      }
      else{
        this.spinner.hide();
        this.router.navigateByUrl('/checkout').then();
        this.toast.error (`Sorry, failed to  book the order`, 'Order Status' ,{
          timeOut: 1500,
          progressBar:true,
          //progressAnimation: 'increasing',
          positionClass: 'toast-top-right',
        });
      }
    });
  }
  private resetServerData(){
    this.cartDataServer = {
      total: 0,
      data: [{
        numInCart: 0,
        product: undefined,
      }],
    };
    this.cartData$.next({... this.cartDataServer});
  }
}

interface OrderResponse {
  order_id: number,
  success: boolean,
  message: string,
  products: [{
    id: string,
    numInCart: string,

  }];
}
